package ru.tsc.panteleev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskEndpointImplTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final TaskDto taskDelete = new TaskDto();

    @NotNull
    private final TaskDto taskSurvivor = new TaskDto();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("testWeb", "testWeb");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(taskDelete);
        save(taskSurvivor);
    }

    @After
    public void clean() {
        clear();
    }

    @SneakyThrows
    private void save(@NotNull final TaskDto taskDto) {
        @NotNull final String url = TASK_URL + "save";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDto);
        mockMvc.perform(
                MockMvcRequestBuilders.post(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void saveTest() {
        @NotNull final TaskDto task = new TaskDto();
        save(task);
        Assert.assertNotNull(findById(task.getId()));
    }

    @SneakyThrows
    public long count() {
        @NotNull final String countUrl = TASK_URL + "count";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(countUrl).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Long.class);
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, count());
    }

    @SneakyThrows
    public Boolean existsById() {
        @NotNull String url = TASK_URL + "existsById/" + taskDelete.getId();
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return false;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Boolean.class);
    }

    @Test
    public void existsByIdTest() {
        Assert.assertEquals(true, existsById());
    }

    @Nullable
    @SneakyThrows
    private TaskDto findById(@NotNull final String id) {
        @NotNull String url = TASK_URL + "findById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDto.class);
    }

    @Test
    public void findByIdTest() {
        @Nullable final TaskDto task = findById(taskDelete.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task.getId());
    }

    @NotNull
    @SneakyThrows
    private List<TaskDto> findAll() {
        @NotNull final String url = TASK_URL + "findAll";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDto[].class));
    }

    @Test
    public void findAllTest() {
        @NotNull final List<TaskDto> taskList = findAll();
        Assert.assertEquals(2, taskList.size());
    }

    @SneakyThrows
    public void delete() {
        @NotNull final String url = TASK_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(taskDelete);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteTest() {
        Assert.assertEquals(2, count());
        delete();
        Assert.assertNull(findById(taskDelete.getId()));
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = TASK_URL + "deleteById/" + taskDelete.getId();
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertEquals(2, count());
        deleteById();
        Assert.assertNull(findById(taskDelete.getId()));
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    public void deleteAll() {
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(taskDelete);
        @NotNull final String url = TASK_URL + "deleteAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(tasks);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteAllTest() {
        Assert.assertEquals(2, count());
        deleteAll();
        Assert.assertNotNull(findById(taskSurvivor.getId()));
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    private void clear() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(TASK_URL + "clear")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(2, count());
        clear();
        Assert.assertEquals(0, count());
    }

}
