package ru.tsc.panteleev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectEndpointImplTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final ProjectDto projectDelete = new ProjectDto();

    @NotNull
    private final ProjectDto projectSurvivor = new ProjectDto();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("testWeb", "testWeb");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(projectDelete);
        save(projectSurvivor);
    }

    @After
    public void clean() {
        clear();
    }

    @SneakyThrows
    private void save(@NotNull final ProjectDto projectDto) {
        @NotNull final String url = PROJECT_URL + "save";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectDto);
        mockMvc.perform(
                MockMvcRequestBuilders.post(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void saveTest() {
        @NotNull final ProjectDto project = new ProjectDto();
        save(project);
        Assert.assertNotNull(findById(project.getId()));
    }

    @SneakyThrows
    public long count() {
        @NotNull final String countUrl = PROJECT_URL + "count";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(countUrl).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Long.class);
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, count());
    }

    @SneakyThrows
    public Boolean existsById() {
        @NotNull String url = PROJECT_URL + "existsById/" + projectDelete.getId();
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return false;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Boolean.class);
    }

    @Test
    public void existsByIdTest() {
        Assert.assertEquals(true, existsById());
    }

    @Nullable
    @SneakyThrows
    private ProjectDto findById(@NotNull final String id) {
        @NotNull String url = PROJECT_URL + "findById/" + id;
        @NotNull final String json =
                mockMvc.perform(
                        MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
                ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDto.class);
    }

    @Test
    public void findByIdTest() {
        @Nullable final ProjectDto project = findById(projectDelete.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), project.getId());
    }

    @NotNull
    @SneakyThrows
    private List<ProjectDto> findAll() {
        @NotNull final String url = PROJECT_URL + "findAll";
        @NotNull final String json = mockMvc.perform(
                MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, ProjectDto[].class));
    }

    @Test
    public void findAllTest() {
        @NotNull final List<ProjectDto> projectList = findAll();
        Assert.assertEquals(2, projectList.size());
    }

    @SneakyThrows
    public void delete() {
        @NotNull final String url = PROJECT_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(projectDelete);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteTest() {
        Assert.assertEquals(2, count());
        delete();
        Assert.assertNull(findById(projectDelete.getId()));
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = PROJECT_URL + "deleteById/" + projectDelete.getId();
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertEquals(2, count());
        deleteById();
        Assert.assertNull(findById(projectDelete.getId()));
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    public void deleteAll() {
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(projectDelete);
        @NotNull final String url = PROJECT_URL + "deleteAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(projects);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(url).content(json).contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteAllTest() {
        Assert.assertEquals(2, count());
        deleteAll();
        Assert.assertNotNull(findById(projectSurvivor.getId()));
        Assert.assertEquals(1, count());
    }

    @SneakyThrows
    private void clear() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(PROJECT_URL + "clear")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(2, count());
        clear();
        Assert.assertEquals(0, count());
    }

}
