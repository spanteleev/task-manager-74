package ru.tsc.panteleev.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserDto extends AbstractModelDto {

    private static final long serialVersionUID = 1;

    @Nullable
    private String login;

    @Nullable
    @Column(name = "password")
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @JsonIgnore
    @XmlTransient
    @Column(nullable = false)
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RoleDto> roles = new ArrayList<>();

    @NotNull
    private Boolean locked = false;

    public UserDto(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDto(
            @Nullable final String login,
            @Nullable final String passwordHash,
            @NotNull final List<RoleDto> roles
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.roles = roles;
    }


}
