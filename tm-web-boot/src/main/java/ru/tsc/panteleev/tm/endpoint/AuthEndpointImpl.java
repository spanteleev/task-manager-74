package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tsc.panteleev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.panteleev.tm.api.service.dto.IUserDtoService;
import ru.tsc.panteleev.tm.dto.model.Result;
import ru.tsc.panteleev.tm.dto.model.UserDto;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.tsc.panteleev.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpointImpl implements IAuthEndpoint {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserDtoService service;

    @NotNull
    @Override
    @WebMethod
    @PostMapping(value = "/login")
    public Result login(
            @NotNull @WebParam(name = "login") final String login,
            @NotNull @WebParam(name = "password") final String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(login, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping(value = "/profile")
    public UserDto profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String login = authentication.getName();
        return service.findByLogin(login);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping(value = "/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}

