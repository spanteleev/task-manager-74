package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.api.endpoint.ITaskDtoEndpoint;
import ru.tsc.panteleev.tm.api.service.dto.ITaskDtoService;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.panteleev.tm.api.endpoint.ITaskDtoEndpoint")
public class TaskEndpointImpl implements ITaskDtoEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @Override
    @WebMethod
    @PutMapping("/create")
    public TaskDto create() {
        return taskDtoService.create(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskDtoService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskDtoService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskDtoService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskDtoService.count(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final TaskDto task
    ) {
        taskDtoService.save(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final TaskDto task
    ) {
        taskDtoService.delete(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        taskDtoService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody final List<TaskDto> tasks
    ) {
        taskDtoService.deleteAll(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskDtoService.clear(UserUtil.getUserId());
    }

}

