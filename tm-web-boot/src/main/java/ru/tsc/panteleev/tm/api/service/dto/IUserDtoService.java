package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.dto.model.UserDto;
import ru.tsc.panteleev.tm.enumerated.RoleType;

public interface IUserDtoService {

    @Transactional
    UserDto createUser(
            @Nullable String login,
            @Nullable String password,
            @Nullable RoleType role
    );

    @NotNull
    UserDto findByLogin(@Nullable String login);

}
