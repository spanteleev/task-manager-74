package ru.tsc.panteleev.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDto;

@NoRepositoryBean
public interface IDtoRepository<M extends AbstractModelDto> extends JpaRepository<M, String> {

}
