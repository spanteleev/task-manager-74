package unit.repository;

import marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.panteleev.tm.configuration.ApplicationConfiguration;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.util.UserUtil;

import java.util.List;
import java.util.UUID;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectDtoRepositoryTest {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final ProjectDto projectDelete = new ProjectDto();

    @NotNull
    private final ProjectDto projectSurvivor = new ProjectDto();

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("testWeb", "testWeb");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectDelete.setUserId(UserUtil.getUserId());
        projectSurvivor.setUserId(UserUtil.getUserId());
        repository.save(projectDelete);
        repository.save(projectSurvivor);
    }

    @After
    public void clean() {
        repository.deleteAll();
    }

    @Test
    public void findByUserIdAndId() {
        @Nullable final ProjectDto projectFind =
                repository.findByUserIdAndId(UserUtil.getUserId(), projectDelete.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(projectFind.getId(), projectDelete.getId());
    }

    @Test
    public void findAllByUserId() {
        List<ProjectDto> projectsFind = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projectsFind);
        Assert.assertEquals(2, projectsFind.size());
    }

    @Test
    public void countByUserId() {
        Assert.assertEquals(2, repository.countByUserId(UserUtil.getUserId()));
    }


    @Test
    public void existsByUserIdAndId() {
        Assert.assertFalse(repository.existsByUserIdAndId(UserUtil.getUserId(), UUID.randomUUID().toString()));
        Assert.assertTrue(repository.existsByUserIdAndId(UserUtil.getUserId(), projectDelete.getId()));
    }

    @Test
    public void deleteByUserIdAndId() {
        repository.deleteByUserIdAndId(UserUtil.getUserId(), UUID.randomUUID().toString());
        Assert.assertEquals(2, repository.countByUserId(UserUtil.getUserId()));
        repository.deleteByUserIdAndId(UserUtil.getUserId(), projectDelete.getId());
        Assert.assertFalse(repository.existsByUserIdAndId(UserUtil.getUserId(), projectDelete.getId()));
    }

    @Test
    public void deleteByUserId() {
        repository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

}
