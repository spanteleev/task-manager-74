package ru.tsc.panteleev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.panteleev.tm.dto.model.CustomUser;
import ru.tsc.panteleev.tm.exception.user.AccessDeniedException;

public interface UserUtil {

    static String getUserId() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @Nullable final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException();
        @NotNull final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}

