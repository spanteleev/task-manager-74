package ru.tsc.panteleev.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.tsc.panteleev.tm.configuration.ApplicationConfiguration;
import ru.tsc.panteleev.tm.configuration.WebApplicationConfiguration;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @NotNull
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                ApplicationConfiguration.class
        };
    }

    @NotNull
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                WebApplicationConfiguration.class
        };
    }

    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[]{
                "/"
        };
    }

}

